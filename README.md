<h2>In variables roles/vars/main.yml change:</h2>

  1. Login to lab: git_user
  2. Access token to the repository and api: git_access_token
  3. Add path to hosts in ansible.cfg
  4. Add user, password and path for ssh: "user_ssh" "pass_ssh"
  5. Add user, password and path for ftp: "user_ftp" "pass_ftp"
